// bwtest provides a simple binary that serves HTTP, returning random data on a
// handler.
package main // import "golang.spin-2.net/bwtest"

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"golang.spin-2.net/bwtest/handlers"
)

var (
	port = flag.Int("port", 8080, "port to listen on")
)

func main() {
	flag.Parse()

	http.Handle("/rand", handlers.RandHandler{})
	http.Handle("/zero", handlers.ZeroHandler{})

	s := http.Server{
		Addr: ":" + strconv.Itoa(*port),
	}
	err := s.ListenAndServe()
	if err != nil {
		fmt.Printf("HTTP serving failed: %s", err)
		os.Exit(1)
	}
}
