// Package handlers provides a few different HTTP handler implementations
// providing useless data.
package handlers

import (
	"crypto/rand"
	"net/http"
	"strconv"
)

const defaultLen = 1 << 20

func extractSize(r *http.Request) (int64, error) {
	lenArg := r.URL.Query().Get("len")
	if lenArg == "" {
		return defaultLen, nil
	}
	lenint, err := strconv.ParseInt(lenArg, 10, 64)
	if err != nil {
		return defaultLen, err
	}
	return lenint, nil
}

// RandHandler implements net.http/Handler and serves a stream from a PRNG (not
// necessarilly a good one, just enough to throw off compression)
type RandHandler struct{}

func (RandHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	length, argErr := extractSize(r)
	if argErr != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid request: " + argErr.Error()))
		return
	}
	w.Header().Add("Content-Length", strconv.FormatInt(length, 10))
	w.WriteHeader(http.StatusOK)
	const stride = 1 << 20

	buf := make([]byte, stride)

	sent := int64(0)

	for sent < length {
		remaining := length - sent
		if int64(len(buf)) > remaining {
			buf = buf[:remaining]
		}
		n, readErr := rand.Reader.Read(buf)
		if readErr != nil {
			return
		}
		sn, wErr := w.Write(buf[:n])
		sent += int64(sn)
		if wErr != nil {
			return
		}
	}
}
