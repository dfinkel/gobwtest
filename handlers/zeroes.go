package handlers // import "golang.spin-2.net/bwtest/handlers"

import (
	"net/http"
	"strconv"
)

// ZeroHandler implements net.http/Handler and serves a stream of zeroes
type ZeroHandler struct{}

func (ZeroHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	length, argErr := extractSize(r)
	if argErr != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid request length: " + argErr.Error()))
		return
	}
	w.Header().Add("Content-Length", strconv.FormatInt(length, 10))
	w.WriteHeader(http.StatusOK)
	const stride = 1 << 20
	buf := make([]byte, stride)

	sent := int64(0)

	for sent < length {
		remaining := length - sent
		if int64(len(buf)) > remaining {
			buf = buf[:remaining]
		}
		sn, wErr := w.Write(buf)
		sent += int64(sn)
		if wErr != nil {
			return
		}
	}

}
